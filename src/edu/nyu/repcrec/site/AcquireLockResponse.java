package edu.nyu.repcrec.site;

import edu.nyu.repcrec.transaction.Transaction;

/**
 * A simple class representing the result of an attempt to acquire locks. It
 * contains the success flag, the transaction blocking the request from getting
 * lock(s) (if any), and the current value of data of which the lock is being
 * requested.
 * 
 * @author Yiwei
 *
 */
public class AcquireLockResponse {
  protected boolean succeessful;
  protected Transaction<?> blocker = null;
  protected Object value = null;

  /**
   * Create an AcquireLockResponse object with an initial success flag.
   * 
   * @param succeessful
   *          the initial success flag
   */
  public AcquireLockResponse(boolean succeessful) {
    this.succeessful = succeessful;
  }

  public boolean isSucceessful() {
    return succeessful;
  }

  public Transaction<?> getBlocker() {
    return blocker;
  }

  public Object getValue() {
    return value;
  }
}
