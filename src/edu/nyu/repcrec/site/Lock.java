package edu.nyu.repcrec.site;

import java.util.Comparator;
import java.util.PriorityQueue;

import edu.nyu.repcrec.transaction.Transaction;

/**
 * A simple class representing the lock of some data item. It is of type read or
 * write, is held by one or more transactions, and is stored in one single site.
 * 
 * @author Yiwei
 *
 */
public class Lock {
  private final LockType type;
  private final String data;
  private final Site<?> site;
  private final PriorityQueue<Transaction<?>> holders;
  private static final Comparator<Transaction<?>> ageComparator = new Comparator<Transaction<?>>() {
    @Override
    public int compare(Transaction<?> o1, Transaction<?> o2) {
      return (int) (o1.getArriveTime() - o2.getArriveTime());
    }
  };

  /**
   * The type of the lock: READ or WRITE.
   * 
   * @author Yiwei
   *
   */
  public enum LockType {
    READ, WRITE
  }

  /**
   * Creates a new Lock object with specific type and an initial holder.
   * 
   * @param type
   *          type of the lock (read or write)
   * @param holder
   *          the initial holder of the lock
   */
  public Lock(LockType type, Site<?> site, String data, Transaction<?> holder) {
    this.type = type;
    this.site = site;
    this.data = data;
    this.holders = new PriorityQueue<Transaction<?>>(100, ageComparator);
    holders.add(holder);
  }

  public LockType getType() {
    return type;
  }

  /**
   * Get the oldest (with the smallest arrival time) holder of the lock.
   * 
   * @return the oldest transaction holding this lock
   */
  public Transaction<?> getOldestHolder() {
    return holders.peek();
  }

  /**
   * Add a holder to the lock.
   * 
   * @param holder
   */
  public void addHolder(Transaction<?> holder) {
    if (!holders.contains(holder)) {
      holders.add(holder);
    }
  }

  /**
   * Remove a holder from the lock. When no one is holding this lock, it will be
   * removed from it site.
   * 
   * @param holder
   */
  public void removeHolder(Transaction<?> holder) {
    holders.remove(holder);
    // If nobody holds the lock, then remove it from it's site.
    if (holders.isEmpty()) {
      site.removeLock(data);
    }
  }

  /**
   * Get the site where this lock is stored.
   * 
   * @return
   */
  public Site<?> getSite() {
    return site;
  }

  /**
   * Get the data ID for which the lock is responsible.
   * 
   * @return
   */
  public String getDataID() {
    return data;
  }

  /**
   * Tell if the lock is held by the transaction.
   * 
   * @param transaction
   * @return true if the transaction is holding the lock
   */
  public boolean isHeldBy(Transaction<?> transaction) {
    return holders.contains(transaction);
  }

  /**
   * Tell if the lock is owned by (exclusively held by) the transaction.
   * 
   * @param transaction
   * @return true if the transaction owns the lock
   */
  public boolean isOwnedBy(Transaction<?> transaction) {
    return holders.peek() == transaction && holders.size() == 1;
  }
}
