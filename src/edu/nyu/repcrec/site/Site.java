package edu.nyu.repcrec.site;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

import edu.nyu.repcrec.data.DataItem;
import edu.nyu.repcrec.data.DataItem.DataItemState;

/**
 * A simple class representing a data-storing site.
 * 
 * @author Yiwei
 *
 * @param <E>
 *          the generic parameter specifying the class of data stored in this
 *          site.
 */
public class Site<E> {
  private final String id;
  private long lastFailTime = -1;
  private SiteState state = SiteState.UP;
  private long earliestVersion;
  private long latestVersion;
  private final HashMap<Long, HashMap<String, DataItem<E>>> versionsOfData;
  private final HashSet<String> replicatedDataIDs;
  private final HashSet<String> exclusiveDataIDs;
  private final HashMap<String, Lock> lockTable;
  private final HashMap<String, DataItemState> dataStates;

  /**
   * The states of site (UP or DOWN).
   * 
   * @author Yiwei
   *
   */
  public enum SiteState {
    UP, DOWN
  }

  /**
   * Create a Site object.
   * 
   * @param sid
   *          the ID of this site
   * @param replicatedDataIDs
   *          the IDs of replicated data stored in this site
   * @param exclusiveDataIDs
   *          the IDs of non-replicated data stored in this site
   * @param initialVersion
   *          initial version of data in this site
   * @param defaultValue
   *          default value of data stored in this site
   */
  public Site(String sid, String[] replicatedDataIDs,
      String[] exclusiveDataIDs, long initialVersion, E defaultValue) {
    this.id = sid;
    this.replicatedDataIDs = new HashSet<String>(
        Arrays.asList(replicatedDataIDs));
    this.exclusiveDataIDs = new HashSet<String>(Arrays.asList(exclusiveDataIDs));

    // Check if replicated and non-replicated IDs intersect;
    if (new HashSet<String>(this.replicatedDataIDs)
        .removeAll(this.exclusiveDataIDs)) {
      throw new IllegalArgumentException(
          "some data marked as both replicated and exclusive.");
    }

    this.lockTable = new HashMap<String, Lock>();
    this.dataStates = new HashMap<String, DataItemState>();

    // Generate first version of data using defaultValue;
    HashMap<String, DataItem<E>> firstVersion = new HashMap<String, DataItem<E>>();
    for (String rID : replicatedDataIDs) {
      firstVersion.put(rID, new DataItem<E>(rID, defaultValue, this));
      this.dataStates.put(rID, initialVersion == 0L ? DataItemState.UP_TO_DATE
          : DataItemState.OUT_OF_DATE);
    }
    for (String eID : exclusiveDataIDs) {
      firstVersion.put(eID, new DataItem<E>(eID, defaultValue, this));
      this.dataStates.put(eID, DataItemState.UP_TO_DATE);
    }
    earliestVersion = initialVersion;
    latestVersion = initialVersion;
    versionsOfData = new HashMap<Long, HashMap<String, DataItem<E>>>();
    versionsOfData.put(initialVersion, firstVersion);
  }

  /**
   * Drop versions of data older than the given version. However, the latest
   * version will always be reserved.
   * 
   * @param version
   *          the version where data older than it will be dropped from this
   *          site
   */
  public void dropVersionsOlderThan(long version) {
    if (version > latestVersion) {
      version = latestVersion;
    }
    for (long i = earliestVersion; i < version; i++) {
      versionsOfData.remove(i);
    }
    earliestVersion = version;
  }

  /**
   * Turn down the site. Also, all its replicated data are out-of-date and
   * cannot be targets of READ operation. The time of this failure is recorded.
   * 
   * @param currentTime
   *          the time of the failure
   */
  public void goDown(long currentTime) {
    this.state = SiteState.DOWN;
    this.lastFailTime = currentTime;
    lockTable.clear();
    for (String rID : replicatedDataIDs) {
      dataStates.put(rID, DataItemState.OUT_OF_DATE);
    }
  }

  /**
   * Bring up the site. Also, all it's non-replicated data 'jumps' to current
   * version, while replicated data in that version appears null.
   */
  public void goUp(long version) {
    this.state = SiteState.UP;
    addVersionOfData(version);
    HashMap<String, DataItem<E>> newVersionOfData = versionsOfData.get(version);
    for (String rID : replicatedDataIDs) {
      newVersionOfData.put(rID, null);
    }
  }

  public String getId() {
    return id;
  }

  /**
   * Get the latest time when this site failed.
   * 
   * @return the latest time when this site failed
   */
  public long getLastFailTime() {
    return lastFailTime;
  }

  /**
   * Tell if this site is up.
   * 
   * @return true if this site is up
   */
  public boolean isUp() {
    return state == SiteState.UP;
  }

  /**
   * Get the state of the site.
   * 
   * @return the state of the site
   */
  public SiteState getState() {
    return state;
  }

  /**
   * Write a new value to the latest version of data in this site. It also
   * brings the data up-to-date.
   * 
   * @param data
   *          the new data being written to this site
   */
  public void writeDataInLatestVersion(DataItem<E> data) {
    HashMap<String, DataItem<E>> versionOfData = versionsOfData
        .get(latestVersion);
    dataStates.put(data.getId(), DataItemState.UP_TO_DATE);
    versionOfData.put(data.getId(), data);
  }

  /**
   * Get the data of a specific version.
   * 
   * @param xid
   *          the ID of the requested data
   * @param version
   *          the requested version
   * @return the DataItem of version
   */
  public DataItem<E> getDataOfVersion(String xid, long version) {
    HashMap<String, DataItem<E>> versionOfData = versionsOfData.get(version);
    if (versionOfData == null) {
      return null;
    }
    return versionOfData.get(xid);
  }

  /**
   * Replace the latest version with a new version.
   * 
   * @param version
   *          the new version
   */
  public void replaceLastestVersionOfData(Long version) {
    if (version <= latestVersion) {
      return;
    }
    HashMap<String, DataItem<E>> versionOfData = versionsOfData
        .get(latestVersion);
    versionsOfData.remove(latestVersion);
    versionsOfData.put(version, versionOfData);
    latestVersion = version;
  }

  /**
   * Add a new version of data.
   * 
   * @param version
   *          the new version
   */
  public void addVersionOfData(Long version) {
    if (version <= latestVersion){
      return;
    }
    HashMap<String, DataItem<E>> versionOfData = new HashMap<String, DataItem<E>>(
        versionsOfData.get(latestVersion));
    versionsOfData.put(version, versionOfData);
    latestVersion = version;
  }

  public void setLock(String xid, Lock lock) {
    assert (lockTable.get(xid) == null);
    lockTable.put(xid, lock);
  }

  public void removeLock(String xid) {
    assert (lockTable.get(xid) != null);
    lockTable.remove(xid);
  }

  public DataItemState getDataState(String xid) {
    return dataStates.get(xid);
  }

  public Lock getLock(String xid) {
    return lockTable.get(xid);
  }

  public DataItem<E> getLatestData(String xid) {
    return getDataOfVersion(xid, latestVersion);
  }
}
