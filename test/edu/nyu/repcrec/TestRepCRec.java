package edu.nyu.repcrec;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Scanner;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;

import edu.nyu.repcrec.data.DataItem;
import edu.nyu.repcrec.transaction.Operation.OperationType;
import edu.nyu.repcrec.transaction.Transaction.TransactionState;
import edu.nyu.repcrec.util.InputParser;

public class TestRepCRec {
  @Rule
  public TestName name = new TestName();
  RepCRec<Integer> dbManager;

  @Before
  public void setUp() throws Exception {
    dbManager = new RepCRec<Integer>(0);
    String[] replicatedDataIDs = { "x2", "x4", "x6", "x8", "x10", "x12", "x14",
        "x16", "x18", "x20", };
    dbManager.addSite("1", replicatedDataIDs, null);
    dbManager.addSite("2", replicatedDataIDs, new String[] { "x1", "x11" });
    dbManager.addSite("3", replicatedDataIDs, null);
    dbManager.addSite("4", replicatedDataIDs, new String[] { "x3", "x13" });
    dbManager.addSite("5", replicatedDataIDs, null);
    dbManager.addSite("6", replicatedDataIDs, new String[] { "x5", "x15" });
    dbManager.addSite("7", replicatedDataIDs, null);
    dbManager.addSite("8", replicatedDataIDs, new String[] { "x7", "x17" });
    dbManager.addSite("9", replicatedDataIDs, null);
    dbManager.addSite("10", replicatedDataIDs, new String[] { "x9", "x19" });
  }

  @Test
  public void testBeginTransactions() {
    System.out.println("=====[" + name.getMethodName() + " starts]=====");
    assertTrue(dbManager.beginTransaction("t1", false));
    assertFalse(dbManager.beginTransaction("t1", false));
  }

  @Test
  public void testEndTransactions() {
    System.out.println("=====[" + name.getMethodName() + " starts]=====");
    dbManager.beginTransaction("t1", false);
    assertTrue(dbManager.endTransaction("t1"));
    assertFalse(dbManager.endTransaction("t2"));
  }

  @Test
  public void testSimpleRead() {
    System.out.println("=====[" + name.getMethodName() + " starts]=====");
    dbManager.beginTransaction("t1", false);
    dbManager.beginTransaction("t2", false);
    dbManager.beginTransaction("t3", false);
    dbManager.beginTransaction("t4", true);
    assertTrue(dbManager.executeOperation(OperationType.READ, "t1", "x1", null));
    assertTrue(dbManager.executeOperation(OperationType.READ, "t1", "x1", null));
    assertTrue(dbManager.executeOperation(OperationType.READ, "t2", "x1", null));
    assertTrue(dbManager.executeOperation(OperationType.READ, "t3", "x2", null));
    assertTrue(dbManager.executeOperation(OperationType.READ, "t4", "x1", null));
    assertTrue(dbManager.executeOperation(OperationType.READ, "t4", "x2", null));
  }

  @Test
  public void testSimpleWrite() {
    System.out.println("=====[" + name.getMethodName() + " starts]=====");
    dbManager.beginTransaction("t1", false);
    dbManager.beginTransaction("t2", false);
    dbManager.beginTransaction("t3", false);
    dbManager.beginTransaction("t4", true);
    assertTrue(dbManager.executeOperation(OperationType.WRITE, "t1", "x1", 113));
    assertTrue(dbManager.executeOperation(OperationType.READ, "t1", "x1", null));
    assertFalse(dbManager
        .executeOperation(OperationType.READ, "t2", "x1", null));
    assertTrue(dbManager.executeOperation(OperationType.WRITE, "t3", "x2", 311));
    assertTrue(dbManager.executeOperation(OperationType.READ, "t4", "x1", null));
    assertTrue(dbManager.executeOperation(OperationType.READ, "t4", "x2", null));
  }

  @Test
  public void testSingleTransactionWithOwnLocks() throws FileNotFoundException {
    System.out.println("=====[" + name.getMethodName() + " starts]=====");
    Scanner sc = new Scanner(new FileInputStream(new File("testScript/"
        + name.getMethodName() + ".txt")));
    while (sc.hasNext()) {
      InputParser.parse(dbManager, sc.nextLine());
    }
    sc.close();
    assertEquals(dbManager.getTransactionState("T1"), "T1: "
        + TransactionState.COMMITTED);
  }

  @Test
  public void testBaseCase1() throws FileNotFoundException {
    initBaseCase(name.getMethodName());
    assertEquals(dbManager.getTransactionState("T1"), "T1: "
        + TransactionState.COMMITTED);
    assertEquals(dbManager.getTransactionState("T2"), "T2: "
        + TransactionState.ABORTED);
    List<DataItem<Integer>> data = dbManager.dump();
    assertTrue(data.get(0).getValue() == 101);
    assertTrue(data.get(1).getValue() == 102);
  }

  @Test
  public void testBaseCase2() throws FileNotFoundException {
    initBaseCase(name.getMethodName());
    assertEquals(dbManager.getTransactionState("T1"), "T1: "
        + TransactionState.COMMITTED);
    assertEquals(dbManager.getTransactionState("T2"), "T2: "
        + TransactionState.COMMITTED);
    List<DataItem<Integer>> data = dbManager.dump();
    assertTrue(data.get(0).getValue() == 101);
    assertTrue(data.get(1).getValue() == 102);
  }

  @Test
  public void testBaseCase3() throws FileNotFoundException {
    initBaseCase(name.getMethodName());
    assertEquals(dbManager.getTransactionState("T1"), "T1: "
        + TransactionState.COMMITTED);
    assertEquals(dbManager.getTransactionState("T2"), "T2: "
        + TransactionState.COMMITTED);
    List<DataItem<Integer>> data = dbManager.dump();
    assertTrue(data.get(7).getValue() == 88);
    assertTrue(data.get(4).getValue() == 91);
  }

  @Test
  public void testBaseCase4() throws FileNotFoundException {
    initBaseCase(name.getMethodName());
    assertEquals(dbManager.getTransactionState("T1"), "T1: "
        + TransactionState.ABORTED);
    assertEquals(dbManager.getTransactionState("T2"), "T2: "
        + TransactionState.COMMITTED);
    List<DataItem<Integer>> data = dbManager.dump();
    assertTrue(data.get(7).getValue() == 88);
  }

  @Test
  public void testBaseCase5() throws FileNotFoundException {
    initBaseCase(name.getMethodName());
    assertEquals(dbManager.getTransactionState("T1"), "T1: "
        + TransactionState.ABORTED);
    assertEquals(dbManager.getTransactionState("T2"), "T2: "
        + TransactionState.COMMITTED);
    List<DataItem<Integer>> data = dbManager.dump();
    assertTrue(data.get(7).getValue() == 88);
    assertTrue(data.get(5).getValue() == 0);
  }

  @Test
  public void testBaseCase6() throws FileNotFoundException {
    initBaseCase(name.getMethodName());
    assertEquals(dbManager.getTransactionState("T1"), "T1: "
        + TransactionState.COMMITTED);
    assertEquals(dbManager.getTransactionState("T2"), "T2: "
        + TransactionState.COMMITTED);
    List<DataItem<Integer>> data = dbManager.dump();
    assertTrue(data.get(7).getValue() == 88);
  }

  @Test
  public void testBaseCase7() throws FileNotFoundException {
    initBaseCase(name.getMethodName());
    assertEquals(dbManager.getTransactionState("T1"), "T1: "
        + TransactionState.COMMITTED);
    assertEquals(dbManager.getTransactionState("T2"), "T2: "
        + TransactionState.COMMITTED);
    List<DataItem<Integer>> data = dbManager.dump();
    assertTrue(data.get(2).getValue() == 33);
  }

  @Test
  public void testBaseCase8() throws FileNotFoundException {
    initBaseCase(name.getMethodName());
    assertEquals(dbManager.getTransactionState("T1"), "T1: "
        + TransactionState.COMMITTED);
    assertEquals(dbManager.getTransactionState("T2"), "T2: "
        + TransactionState.COMMITTED);
    assertEquals(dbManager.getTransactionState("T3"), "T3: "
        + TransactionState.RUNNING);
    List<DataItem<Integer>> data = dbManager.dump();
    assertTrue(data.get(2).getValue() == 33);
  }

  @Test
  public void testBaseCase9() throws FileNotFoundException {
    initBaseCase(name.getMethodName());
    assertEquals(dbManager.getTransactionState("T1"), "T1: "
        + TransactionState.COMMITTED);
    assertEquals(dbManager.getTransactionState("T2"), "T2: "
        + TransactionState.COMMITTED);
    assertEquals(dbManager.getTransactionState("T3"), "T3: "
        + TransactionState.COMMITTED);
    List<DataItem<Integer>> data = dbManager.dump();
    assertTrue(data.get(1).getValue() == 22);
    assertTrue(data.get(3).getValue() == 44);
  }

  @Test
  public void testBaseCase10() throws FileNotFoundException {
    initBaseCase(name.getMethodName());
    assertEquals(dbManager.getTransactionState("T1"), "T1: "
        + TransactionState.COMMITTED);
    assertEquals(dbManager.getTransactionState("T2"), "T2: "
        + TransactionState.COMMITTED);
    assertEquals(dbManager.getTransactionState("T3"), "T3: "
        + TransactionState.ABORTED);
    List<DataItem<Integer>> data = dbManager.dump();
    assertTrue(data.get(3).getValue() == 44);
  }

  @Test
  public void testBaseCase11() throws FileNotFoundException {
    initBaseCase(name.getMethodName());
    assertEquals(dbManager.getTransactionState("T1"), "T1: "
        + TransactionState.COMMITTED);
    assertEquals(dbManager.getTransactionState("T2"), "T2: "
        + TransactionState.ABORTED);
    List<DataItem<Integer>> data = dbManager.dump();
    assertTrue(data.get(1).getValue() == 0);
  }

  @Test
  public void testBaseCase12() throws FileNotFoundException {
    initBaseCase(name.getMethodName());
    assertEquals(dbManager.getTransactionState("T1"), "T1: "
        + TransactionState.COMMITTED);
    assertEquals(dbManager.getTransactionState("T2"), "T2: "
        + TransactionState.COMMITTED);
    List<DataItem<Integer>> data = dbManager.dump();
    assertTrue(data.get(1).getValue() == 10);
  }

  @Test
  public void testAdvancedCase1() throws FileNotFoundException {
    initBaseCase(name.getMethodName());
    assertEquals(dbManager.getTransactionState("T1"), "T1: "
        + TransactionState.COMMITTED);
    assertEquals(dbManager.getTransactionState("T2"), "T2: "
        + TransactionState.COMMITTED);
    assertEquals(dbManager.getTransactionState("T3"), "T3: "
        + TransactionState.COMMITTED);
    assertEquals(dbManager.getTransactionState("T4"), "T4: "
        + TransactionState.ABORTED);
    List<DataItem<Integer>> data = dbManager.dump();
    assertTrue(data.get(0).getValue() == 101);
    assertTrue(data.get(1).getValue() == 102);
    assertTrue(data.get(2).getValue() == 203);
    assertTrue(data.get(3).getValue() == 304);
  }

  @Test
  public void testAdvancedCase2() throws FileNotFoundException {
    initBaseCase(name.getMethodName());
    assertEquals(dbManager.getTransactionState("T1"), "T1: "
        + TransactionState.COMMITTED);
    assertEquals(dbManager.getTransactionState("T2"), "T2: "
        + TransactionState.COMMITTED);
    assertEquals(dbManager.getTransactionState("T3"), "T3: "
        + TransactionState.COMMITTED);
    assertEquals(dbManager.getTransactionState("T4"), "T4: "
        + TransactionState.COMMITTED);
    List<DataItem<Integer>> data = dbManager.dump();
    assertTrue(data.get(0).getValue() == 101);
    assertTrue(data.get(1).getValue() == 102);
  }

  @Test
  public void testAdvancedCase3() throws FileNotFoundException {
    initBaseCase(name.getMethodName());
    assertEquals(dbManager.getTransactionState("T1"), "T1: "
        + TransactionState.ABORTED);
    assertEquals(dbManager.getTransactionState("T2"), "T2: "
        + TransactionState.COMMITTED);
    assertEquals(dbManager.getTransactionState("T3"), "T3: "
        + TransactionState.COMMITTED);
    assertEquals(dbManager.getTransactionState("T4"), "T4: "
        + TransactionState.COMMITTED);
    List<DataItem<Integer>> data = dbManager.dump();
    assertTrue(data.get(7).getValue() == 88);
    assertTrue(data.get(9).getValue() == 100);
    assertTrue(data.get(8).getValue() == 99);
  }

  private void initBaseCase(String methodName) throws FileNotFoundException {
    System.out.println("=====[" + methodName + " starts]=====");
    Scanner sc = new Scanner(new FileInputStream(new File("testScript/"
        + methodName + ".txt")));
    while (sc.hasNext()) {
      InputParser.parse(dbManager, sc.nextLine());
    }
    sc.close();
  }
}
