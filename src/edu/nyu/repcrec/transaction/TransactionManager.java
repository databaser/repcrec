package edu.nyu.repcrec.transaction;

import java.util.HashMap;
import java.util.LinkedList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * A transaction manager which stores all transactions' information, and keeps
 * track of waiting transactions and read-only transactions for the convenience
 * of the database manager.
 * 
 * @author Yiwei
 *
 * @param <E>
 *          the generic parameter specifying the class of data stored associated
 *          with transactions.
 */
public class TransactionManager<E> {
  private final HashMap<String, Transaction<E>> transactions = new HashMap<String, Transaction<E>>();
  private final LinkedList<Transaction<E>> waitingTransactions = new LinkedList<Transaction<E>>();
  private final LinkedList<Transaction<E>> readonlyTransactions = new LinkedList<Transaction<E>>();

  private static final Logger logger = LogManager
      .getLogger("repcrec.transaction");

  public boolean beginTransaction(String tid, long time, long versionId,
      boolean isReadOnly) {
    if (transactions.get(tid) != null) {
      logger.error("transaction '" + tid + "' already exists.");
      return false;
    } else {
      Transaction<E> transaction = new Transaction<E>(tid, time, versionId,
          isReadOnly);
      transactions.put(tid, transaction);
      if (isReadOnly) {
        readonlyTransactions.add(transaction);
      }
      return true;
    }
  }

  public Transaction<E> getTransaction(String tid) {
    return transactions.get(tid);
  }

  public void addToWaitingTransaction(Transaction<E> transaction) {
    waitingTransactions.add(transaction);
  }

  public Transaction<E> getEarliestReadOnly() {
    return readonlyTransactions.peekFirst();
  }

  public Transaction<E> getOldestReadOnly() {
    return readonlyTransactions.peekLast();
  }

  public boolean checkEarliestAndPop(Transaction<E> transaction) {
    boolean isEarliest = false;
    if (transaction == readonlyTransactions.peekFirst()) {
      isEarliest = true;
    }
    readonlyTransactions.remove(transaction);
    return isEarliest;
  }

  public LinkedList<Transaction<E>> getWaitingTransactions() {
    return waitingTransactions;
  }
}
