package edu.nyu.repcrec.data;

import edu.nyu.repcrec.site.Site;

/**
 * A simple class representing the actual data stored in sites. It's generic and
 * immutable.
 * 
 * @author Yiwei
 *
 * @param <E>
 *          the generic parameter specifying the class of actual data stored in
 *          this object
 */
public class DataItem<E> {
  private final String id;
  private final E value;
  private final Site<E> site;

  /**
   * Create a new DataItem.
   * 
   * @param xid
   *          the ID of this DataItem
   * @param value
   *          the value of actual data
   * @param site
   *          the site where this DataItem is stored
   */
  public DataItem(String xid, E value, Site<E> site) {
    this.id = xid;
    this.value = value;
    this.site = site;
  }

  /**
   * States of DataItem.
   * 
   * @author Yiwei
   *
   */
  public enum DataItemState {
    UP_TO_DATE, OUT_OF_DATE, UNAVAILABLE
  }

  public String getId() {
    return id;
  }

  public E getValue() {
    return value;
  }

  public Site<E> getSite() {
    return site;
  }
}
