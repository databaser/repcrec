package edu.nyu.repcrec.transaction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import edu.nyu.repcrec.data.DataItem;
import edu.nyu.repcrec.site.Lock;
import edu.nyu.repcrec.site.Site;

/**
 * A simple class representing a transaction. It holds all information of a
 * transaction, and is stored in Transaction Manager.
 * 
 * @author Yiwei
 *
 * @param <E>
 *          the generic parameter specifying the class of data associated with
 *          the transaction.
 */
public class Transaction<E> {
  private final String id;
  private final long arriveTime;
  private final boolean isReadOnly;
  private final long version;
  private TransactionState state;
  private final LinkedList<Operation<E>> operationList;
  private final HashMap<Site<E>, Long> accessedSites;
  private final List<DataItem<E>> writtenDataList;
  private LinkedList<Lock> acquiredLocks;

  public enum TransactionState {
    RUNNING, WAITING, ENDED, ABORTED, COMMITTED
  }

  public Transaction(String id, long time, long version, boolean isReadOnly) {
    this.id = id;
    this.arriveTime = time;
    this.isReadOnly = isReadOnly;
    this.version = version;
    this.state = TransactionState.RUNNING;
    this.operationList = new LinkedList<Operation<E>>();
    this.accessedSites = new HashMap<Site<E>, Long>();
    this.writtenDataList = new ArrayList<DataItem<E>>();
    this.acquiredLocks = new LinkedList<Lock>();
  }

  public void end() {
    this.state = TransactionState.ENDED;
  }

  public void addOperation(Operation<E> op) {
    if (state != TransactionState.ENDED) {
      operationList.add(op);
    }
  }

  public Operation<E> getLastOperation() {
    return operationList.peekLast();
  }

  public long getVersion() {
    return version;
  }

  public long getArriveTime() {
    return arriveTime;
  }

  public TransactionState getState() {
    return state;
  }

  public List<DataItem<E>> getWrittenDataList() {
    return writtenDataList;
  }

  public boolean isReadOnly() {
    return isReadOnly;
  }

  public String getId() {
    return id;
  }

  public void setState(TransactionState state) {
    this.state = state;
  }

  public HashMap<Site<E>, Long> getAccessedSites() {
    return accessedSites;
  }

  public void addAccessedSites(Site<E> site, long time) {
    if (!accessedSites.containsKey(site)){
      accessedSites.put(site, time);
    }
  }

  public void acquiredLock(Lock lock) {
    acquiredLocks.add(lock);
  }

  public List<Lock> popAcquiredLocks() {
    List<Lock> locks = acquiredLocks;
    acquiredLocks = new LinkedList<Lock>();
    return locks;
  }
}
