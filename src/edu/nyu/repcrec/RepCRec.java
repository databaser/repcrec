package edu.nyu.repcrec;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.nyu.repcrec.data.DataItem;
import edu.nyu.repcrec.site.AcquireLockResponse;
import edu.nyu.repcrec.site.Lock;
import edu.nyu.repcrec.site.Site;
import edu.nyu.repcrec.site.SiteManager;
import edu.nyu.repcrec.transaction.Operation;
import edu.nyu.repcrec.transaction.Operation.OperationType;
import edu.nyu.repcrec.transaction.Transaction;
import edu.nyu.repcrec.transaction.Transaction.TransactionState;
import edu.nyu.repcrec.transaction.TransactionManager;

/**
 * A simple Replicated Concurrency Control and Recovery (RepCRec) database
 * management system.
 * 
 * @author Yiwei
 * @author Daran
 *
 * @param <E>
 *          The generic parameter identifying what class of data is stored in
 *          this database
 */
public class RepCRec<E> {
  private long currentTime;
  private final E defaultValue;
  private final TransactionManager<E> transactionManager;
  private final SiteManager<E> siteManager;
  private static final Logger logger = LogManager.getLogger("repcrec");

  /**
   * Creates a RepCRec object.
   * 
   * @param defaultValue
   *          the default value used by all data items in all sites.
   */
  public RepCRec(E defaultValue) {
    this.currentTime = 0;
    this.defaultValue = defaultValue;
    this.transactionManager = new TransactionManager<E>();
    this.siteManager = new SiteManager<E>(this);
  }

  /**
   * Add a site for data storage.
   * 
   * @param sid
   *          the ID of the site, must be unique
   * @param replicatedDataIDs
   *          IDs of replicated data stored in this site
   * @param exclusiveDataIDs
   *          IDs of non-replicated data stored in this site
   */
  public void addSite(String sid, String[] replicatedDataIDs,
      String[] exclusiveDataIDs) {
    replicatedDataIDs = replicatedDataIDs == null ? new String[0]
        : replicatedDataIDs;
    exclusiveDataIDs = exclusiveDataIDs == null ? new String[0]
        : exclusiveDataIDs;
    siteManager.addSite(sid, replicatedDataIDs, exclusiveDataIDs, defaultValue);
  }

  /**
   * Begins a transaction.
   * 
   * @param tid
   *          the ID of the transaction, must be unique
   * @param isReadOnly
   *          specify whether this transaction is a read-only transaction
   * @return true if successful
   */
  public boolean beginTransaction(String tid, boolean isReadOnly) {
    if (tid == null || tid.isEmpty()) {
      logger.error("transaction id cannot be null or empty");
      return false;
    } else {
      return transactionManager.beginTransaction(tid, currentTime,
          siteManager.getCurrentVersion(), isReadOnly);
    }
  }

  /**
   * Ends a transaction. By doing this, the transaction will try do commit all
   * its changes to the database.
   * 
   * @param tid
   *          the ID of the transaction
   * @return true if successful
   */
  public boolean endTransaction(String tid) {
    if (tid == null || tid.isEmpty()) {
      logger.error("transaction id cannot be null or empty");
      return false;
    } else {
      Transaction<E> transaction = transactionManager.getTransaction(tid);
      if (transaction == null) {
        logger.error("transaction '" + tid + "' doesn't exist.");
        return false;
      }
      if (transaction.isReadOnly()) {
        commitReadOnly(transaction);
      } else {
        commitReadWrite(transaction);
      }
      return true;
    }
  }

  /**
   * Executes an operation.
   * 
   * @param type
   *          the type (read or write) of the operation
   * @param tid
   *          the ID of the transaction issuing this operation
   * @param xid
   *          the ID of the data item related to this operation
   * @param value
   *          the new value if the operation is a write operation.
   * @return true if successful
   */
  public boolean executeOperation(OperationType type, String tid, String xid,
      E value) {
    Transaction<E> transaction = transactionManager.getTransaction(tid);
    if (transaction == null) {
      logger.error("transaction '" + tid + "' doesn't exist.");
      return false;
    }
    if (!siteManager.dataExists(xid)) {
      logger.error("data '" + xid + "' doesn't exist.");
      return false;
    }
    Operation<E> op = Operation.getInstance(transaction, type, xid, value);
    transaction.addOperation(op);
    return executeLastOperation(transaction);
  }

  /**
   * Simulates a site failure.
   * 
   * @param sid
   *          the ID of the site
   * @return true if successful
   */
  public boolean siteFail(String sid) {
    return siteManager.siteFail(sid, currentTime);
  }

  /**
   * Simulates a site recovery. After that, the system will try to re-issue all
   * waiting transactions.
   * 
   * @param sid
   * @return true if successful
   */
  public boolean siteRecover(String sid) {
    boolean successful = siteManager.siteRecover(sid);
    if (successful) {
      redoWaitingTransactions();
    }
    return successful;
  }

  /**
   * Dumps the newest version of all data items.
   * 
   * @return a list of all data items
   */
  public List<DataItem<E>> dump() {
    return siteManager.dump();
  }

  /**
   * Dumps a specific version of all data items.
   * 
   * @param version
   *          the version of data wanted
   * @return a list of all data items
   */
  public List<DataItem<E>> dumpVersion(long version) {
    return siteManager.dumpVersion(version);
  }

  public HashMap<Long, List<DataItem<E>>> dumpAll() {
    return siteManager.dumpAll();
  }

  public E getLatesDataValue(String xid) {
    DataItem<E> data = siteManager.getLatestData(xid);
    if (data == null) {
      return null;
    } else {
      return siteManager.getLatestData(xid).getValue();
    }
  }

  /**
   * The state of a site. Only for debug use and subject to removal without
   * notification.
   * 
   * @param sid
   *          the ID of the site
   * @return the string representation of site state
   */
  public String getSiteState(String sid) {
    Site<E> s = siteManager.getSite(sid);
    if (s == null) {
      return sid + " doesn't exist.";
    } else {
      return sid + ": " + s.getState().toString();
    }
  }

  /**
   * The state of a transaction. Only for debug use and subject to removal
   * without notification.
   * 
   * @param tid
   *          the ID of the transaction
   * @return the string representation of transaction state
   */
  public String getTransactionState(String tid) {
    Transaction<E> t = transactionManager.getTransaction(tid);
    if (t == null) {
      return tid + " doesn't exist.";
    } else {
      return tid + ": " + t.getState().toString();
    }
  }

  /**
   * Move internal time ahead by 1. In real-life system, this may probably be
   * replaced by system time in millisecond.
   */
  public void timeTick() {
    currentTime++;
  }

  /**
   * Get internal time of the system. In real-life system, this may probably be
   * replaced by system time in millisecond.
   * 
   * @return a long number representing the time
   */
  public long getCurrentTime() {
    return currentTime;
  }

  private void purgeOldVersion() {
    Transaction<E> newEarliest = transactionManager.getEarliestReadOnly();
    if (newEarliest != null) {
      siteManager.dropVersionOlderThan(newEarliest.getVersion());
    } else {
      siteManager.dropVersionOld();
    }
  }

  private void commitReadOnly(Transaction<E> transaction) {
    assert (transaction.isReadOnly());
    if (transactionManager.checkEarliestAndPop(transaction)) {
      purgeOldVersion();
    }
    transaction.setState(TransactionState.COMMITTED);
    logger.info(transaction.getId() + " successfully committed.");
  }

  private void commitReadWrite(Transaction<E> transaction) {
    assert (!transaction.isReadOnly());
    boolean canCommit = (transaction.getState() != TransactionState.ABORTED);
    // Check if accessed sites ever fail after first access time;
    if (canCommit) {
      HashMap<Site<E>, Long> accessedSites = transaction.getAccessedSites();
      for (Site<E> site : accessedSites.keySet()) {
        if (site.getLastFailTime() > accessedSites.get(site)) {
          canCommit = false;
          break;
        }
      }
    }
    // Update written data to sites;
    if (canCommit) {
      Transaction<E> oldestReadOnly = transactionManager.getOldestReadOnly();
      if (oldestReadOnly == null) {
        siteManager.commitData(transaction.getWrittenDataList(), currentTime,
            -1L);
      } else {
        siteManager.commitData(transaction.getWrittenDataList(), currentTime,
            oldestReadOnly.getVersion());
      }
      logger.info(transaction.getId() + " successfully committed.");
      transaction.setState(TransactionState.COMMITTED);
    } else {
      logger.info(transaction.getId() + " aborted due to site failure");
      transaction.setState(TransactionState.ABORTED);
    }
    if (!canCommit) {
      logger.info(transaction.getId() + " ends aborted.");
    }
    releaseAllLocks(transaction);
  }

  /**
   * Release all locks the transaction possessed. After that, the system will
   * try to re-issue all waiting transactions.
   * 
   * @param transaction
   */
  private void releaseAllLocks(Transaction<E> transaction) {
    List<Lock> locks = transaction.popAcquiredLocks();
    for (Lock lock : locks) {
      lock.removeHolder(transaction);
    }
    redoWaitingTransactions();
  }

  private void redoWaitingTransactions() {
    LinkedList<Transaction<E>> waitingTransactions = transactionManager
        .getWaitingTransactions();
    int count = waitingTransactions.size();
    while (count > 0) {
      Transaction<E> transaction = waitingTransactions.poll();
      if (executeLastOperation(transaction)) {
        logger.info(transaction.getId() + " resumed.");
      }
      count--;
    }
  }

  private boolean executeLastOperation(Transaction<E> transaction) {
    Operation<E> op = transaction.getLastOperation();
    boolean succeed = false;
    if (op.getType() == OperationType.READ) {
      succeed = excuteRead(transaction, op.getDataID());
    } else if (op.getType() == OperationType.WRITE) {
      succeed = executeWrite(transaction, op.getDataID(), op.getValue());
    }
    if (transaction.getState() == TransactionState.ABORTED) {
      releaseAllLocks(transaction);
    }
    return succeed;
  }

  private boolean executeWrite(Transaction<E> transaction, String xid, E value) {
    if (transaction.getState() == TransactionState.ABORTED) {
      logger.info("operation canceled: transaction '" + transaction.getId()
          + "' is aborted.");
      return false;
    }
    if (value == null){
      logger.error("Cannot write null value.");
      return false;
    }
    // Not aborted yet, go on;
    AcquireLockResponse response = siteManager.acquireWriteLock(xid,
        transaction);
    if (!response.isSucceessful()) {
      if (response.getBlocker() == null) {
        logger.info(transaction.getId() + " waiting for site(s).");
        sendToWaitlist(transaction);
      } else {
        if (olderThan(response.getBlocker(), transaction)) {
          logger.info(transaction.getId() + " aborted by "
              + response.getBlocker().getId() + ".");
          abortTransaction(transaction);
        } else {
          logger.info(transaction.getId() + " waiting for lock(s).");
          sendToWaitlist(transaction);
        }
      }
    } else {
      logger.info(transaction.getId() + " writes " + xid + ": "
          + response.getValue() + " -> " + value.toString() + ".");
      transaction.getWrittenDataList().add(new DataItem<E>(xid, value, null));
    }
    return response.isSucceessful();
  }

  private boolean excuteRead(Transaction<E> transaction, String xid) {
    if (transaction.getState() == TransactionState.ABORTED) {
      logger.info("operation canceled: transaction '" + transaction.getId()
          + "' is aborted.");
      return false;
    }
    // Not aborted yet, go on;
    if (transaction.isReadOnly()) {
      // Read-only transaction;
      DataItem<E> data = siteManager.getDataOfVersion(xid,
          transaction.getVersion());
      if (data != null) {
        logger.info(transaction.getId() + " reads " + xid + ": "
            + data.getValue().toString() + ".");
        return true;
      } else {
        transaction.setState(TransactionState.WAITING);
        transactionManager.addToWaitingTransaction(transaction);
        return false;
      }
    } else {
      AcquireLockResponse response = siteManager.acquireReadLock(xid,
          transaction);
      if (!response.isSucceessful()) {
        if (response.getBlocker() == null) {
          // fail because no available sites
          logger.info(transaction.getId() + " waiting for site(s).");
          sendToWaitlist(transaction);
        } else {
          // or fail because can't get locks;
          if (olderThan(response.getBlocker(), transaction)) {
            logger.info(transaction.getId() + " aborted by "
                + response.getBlocker().getId() + ".");
            abortTransaction(transaction);
          } else {
            logger.info(transaction.getId() + " waiting for lock(s).");
            sendToWaitlist(transaction);
          }
        }
      } else {
        logger.info(transaction.getId() + " reads " + xid + ": "
            + response.getValue().toString() + ".");
      }
      return response.isSucceessful();
    }
  }

  private void abortTransaction(Transaction<E> transaction) {
    transaction.setState(TransactionState.ABORTED);
  }

  private void sendToWaitlist(Transaction<E> transaction) {
    transaction.setState(TransactionState.WAITING);
    transactionManager.addToWaitingTransaction(transaction);
  }

  private boolean olderThan(Transaction<?> a, Transaction<?> b) {
    return a.getArriveTime() <= b.getArriveTime();
  }
}
