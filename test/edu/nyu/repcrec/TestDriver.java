package edu.nyu.repcrec;

import java.io.FileNotFoundException;
import java.util.Scanner;

import edu.nyu.repcrec.util.InputParser;

/**
 * A test driver class which receive input from file, and print output directly
 * to stdout.The environment is set as described in project description:<br/>
 * <br/>
 * 1. data type is Integer,<br/>
 * 2. default value is 0,<br/>
 * 3. 10 data sites,<br/>
 * 4. replicated data with even indices,<br/>
 * 5. non-replicated data with odd indices.
 * 
 * @author Yiwei
 *
 */
public class TestDriver {
  public static void main(String[] args) throws FileNotFoundException {
    Scanner sc = null;
    if (args.length == 0) {
      sc = new Scanner(System.in);
    } else {
      sc = new Scanner(System.in);
    }
    // Creates a database manager: data type is Integer and default value is 0;
    RepCRec<Integer> dbManager = new RepCRec<Integer>(0);

    // Initialize data items in database;
    String[] replicatedDataIDs = { "x2", "x4", "x6", "x8", "x10", "x12", "x14",
        "x16", "x18", "x20", };
    dbManager.addSite("1", replicatedDataIDs, null);
    dbManager.addSite("2", replicatedDataIDs, new String[] { "x1", "x11" });
    dbManager.addSite("3", replicatedDataIDs, null);
    dbManager.addSite("4", replicatedDataIDs, new String[] { "x3", "x13" });
    dbManager.addSite("5", replicatedDataIDs, null);
    dbManager.addSite("6", replicatedDataIDs, new String[] { "x5", "x15" });
    dbManager.addSite("7", replicatedDataIDs, null);
    dbManager.addSite("8", replicatedDataIDs, new String[] { "x7", "x17" });
    dbManager.addSite("9", replicatedDataIDs, null);
    dbManager.addSite("10", replicatedDataIDs, new String[] { "x9", "x19" });

    while (sc.hasNext()) {
      InputParser.parse(dbManager, sc.nextLine());
    }
    sc.close();
  }
}
