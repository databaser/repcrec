# RepCRec #

RepCRec (Replicated Concurrency Control and Recovery) is a simplified distributed database, complete with multiversion concurrency control, deadlock avoidance, replication, and failure recovery.

### What does it include? ###

* A well documented and commented database system written in pure Java.
* A test driver demonstrating how the system works and,
* Unit tests focusing on good coverage and corner cases.
