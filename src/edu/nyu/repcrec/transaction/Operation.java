package edu.nyu.repcrec.transaction;

/**
 * 
 * @author Yiwei
 *
 * @param <E>
 */
public class Operation<E> {
  private final Transaction<E> transaction;
  private final OperationType type;
  private final String data;
  private final E value;

  public enum OperationType {
    READ, WRITE
  }

  private Operation(Transaction<E> transaction, OperationType type,
      String data, E value) {
    this.transaction = transaction;
    this.type = type;
    this.data = data;
    this.value = value;
  }

  public static <E> Operation<E> getInstance(Transaction<E> transaction,
      OperationType type, String data, E value) {
    return new Operation<E>(transaction, type, data, value);
  }

  public OperationType getType() {
    return type;
  }

  public Transaction<E> getTransaction() {
    return transaction;
  }

  public String getDataID() {
    return data;
  }

  public E getValue() {
    return value;
  }
}
