package edu.nyu.repcrec.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.math.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.nyu.repcrec.RepCRec;
import edu.nyu.repcrec.data.DataItem;
import edu.nyu.repcrec.transaction.Operation.OperationType;

/**
 * A simple parser class with static function, so that one can use the RepCRec
 * database system with the input format given in project description.
 * 
 * @author Yiwei
 *
 */
public class InputParser {
  private static final Pattern pattern = Pattern
      .compile("([^\\(]*)\\(([^\\)]*)\\)");
  private static final Logger logger = LogManager.getLogger("repcrec.parse");

  /**
   * Private constructor to prevent instantiation.
   */
  private InputParser() {
  };

  private static String getOperator(String command) {
    Matcher matcher = pattern.matcher(command);
    if (matcher.find()) {
      return matcher.group(1).toLowerCase();
    }
    return null;
  }

  private static String getOperand(String command) {
    Matcher matcher = pattern.matcher(command);
    if (matcher.find()) {
      return matcher.group(2);
    }
    return null;
  }

  /**
   * Parse one line of input. The input is expected to be in the format
   * specified in ADB project description.
   * 
   * @param manager
   *          the RepCRec object created beforehand
   * @param line
   *          one line of input
   */
  public static boolean parse(RepCRec<Integer> manager, String line) {
    if (line.startsWith("//") || line.trim().isEmpty()) {
      return false;
    }
    String[] commands = line.split(";");
    trimStringArray(commands);
    boolean result = false;
    for (String command : commands) {
      logger.debug("command: " + command);
      String operator = InputParser.getOperator(command);
      String operand = InputParser.getOperand(command);
      if (operator == null || operator.isEmpty()) {
        logger.error("wrong command: '" + command + "'");
        return false;
      }
      switch (operator) {
      // Transaction operations;
      case "begin":
        result = manager.beginTransaction(operand, false);
        break;
      case "beginro":
        result = manager.beginTransaction(operand, true);
        break;
      case "end":
        result = manager.endTransaction(operand);
        break;
      // Data operations;
      case "w": {
        String[] parameters = operand.split(",");
        if (parameters.length != 3) {
          logger.error("wrong write command format: " + command);
        } else {
          trimStringArray(parameters);
          result = manager.executeOperation(OperationType.WRITE, parameters[0],
              parameters[1], Integer.parseInt(parameters[2]));
        }
        break;
      }
      case "r": {
        String[] parameters = operand.split(",");
        if (parameters.length != 2) {
          logger.error("wrong read command format: " + command);
        } else {
          trimStringArray(parameters);
          result = manager.executeOperation(OperationType.READ, parameters[0],
              parameters[1], null);
        }
        break;
      }
      // Site operations;
      case "fail":
        result = manager.siteFail(operand);
        break;
      case "recover":
        result = manager.siteRecover(operand);
        break;
      // Dump data;
      case "dump": {
        List<DataItem<Integer>> dataDump = null;
        if (operand.isEmpty()) {
          dataDump = manager.dump();
        } else {
          Long version = NumberUtils.toLong(operand, -1L);
          if (version < 0) {
            logger.error(operand + " is not a valid version number.");
          } else {
            dataDump = manager.dumpVersion(version);
          }
        }
        if (dataDump != null) {
          result = true;
          printData(dataDump);
        }
      }
        break;
      case "dumpall": {
        HashMap<Long, List<DataItem<Integer>>> dataDump = null;
        dataDump = manager.dumpAll();
        if (dataDump != null) {
          result = true;
          List<Long> versions = new ArrayList<Long>(dataDump.keySet());
          Collections.sort(versions);
          for (Long version : versions) {
            System.out.println("Version " + version + ":");
            printData(dataDump.get(version));
          }
        }
      }
        break;
      // Check status;
      case "check":
        if (operand.startsWith("T")) {
          logger.info(manager.getTransactionState(operand));
        } else {
          logger.info(manager.getSiteState(operand));
        }
        result = true;
        break;
      // Quit command;
      case "quit":
        System.exit(0);
        break;
      default:
        result = false;
        logger.error("'" + operator + "' command is not supported.");
      }
    }
    manager.timeTick();
    return result;
  }

  private static String[] trimStringArray(String[] strArray) {
    for (int i = 0; i < strArray.length; i++) {
      strArray[i] = strArray[i].trim();
    }
    return strArray;
  }

  private static void printData(List<DataItem<Integer>> dataDump) {
    int count = 1;
    for (DataItem<?> data : dataDump) {
      System.out.format("%3s:%5d\t", data.getId(), data.getValue());
      if (count % 4 == 0) {
        System.out.println();
      }
      count++;
    }
  }
}