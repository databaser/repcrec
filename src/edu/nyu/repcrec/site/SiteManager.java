package edu.nyu.repcrec.site;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.nyu.repcrec.RepCRec;
import edu.nyu.repcrec.data.DataItem;
import edu.nyu.repcrec.data.DataItem.DataItemState;
import edu.nyu.repcrec.site.Lock.LockType;
import edu.nyu.repcrec.transaction.Transaction;

/**
 * A site manager which handles lock request, data version control, etc. Most of
 * the interaction with sites should be invoked through it.
 * 
 * @author Yiwei & Daran
 *
 * @param <E>
 *          the generic parameter specifying the class of data stored in sites.
 */
public class SiteManager<E> {
  private final LinkedList<Long> versions = new LinkedList<Long>();
  private final HashMap<String, Site<E>> sites = new HashMap<String, Site<E>>();
  private final HashMap<String, List<Site<E>>> dataToSites = new HashMap<String, List<Site<E>>>();
  private final RepCRec<E> manager;
  private static final Comparator<String> dataIDComparator = new Comparator<String>() {
    @Override
    public int compare(String s1, String s2) {
      return Integer.parseInt(s1.substring(1))
          - Integer.parseInt(s2.substring(1));
    }
  };
  private static final Logger logger = LogManager.getLogger("repcrec.site");

  public SiteManager(RepCRec<E> manager) {
    this.manager = manager;
    versions.add(0L);
  }

  /**
   * Add a site to the database.
   * 
   * @param sid
   * @param replicatedDataIDs
   * @param exclusiveDataIDs
   * @param defaultValue
   * @return true if successfully added
   */
  public boolean addSite(String sid, String[] replicatedDataIDs,
      String[] exclusiveDataIDs, E defaultValue) {
    if (sites.get(sid) != null) {
      logger.error("Site: " + sid + " already exists.");
      return false;
    }

    // Check if exclusive id exists in sites;
    for (String exclusiveDataID : exclusiveDataIDs) {
      if (dataToSites.containsKey(exclusiveDataID)) {
        logger.error("Exclusive data ID: " + exclusiveDataID + " is used");
        return false;
      }
    }
    Site<E> site = new Site<E>(sid, replicatedDataIDs, exclusiveDataIDs,
        getCurrentVersion(), defaultValue);
    sites.put(sid, site);

    // Add IDs to dataID-to-sites map;
    for (String eID : exclusiveDataIDs) {
      ArrayList<Site<E>> list = new ArrayList<Site<E>>();
      list.add(site);
      dataToSites.put(eID, list);
    }
    for (String rID : replicatedDataIDs) {
      List<Site<E>> list = dataToSites.get(rID);
      if (list == null) {
        list = new ArrayList<Site<E>>();
        dataToSites.put(rID, list);
      }
      list.add(site);
    }
    return true;
  }

  public Site<E> getSite(String sid) {
    return sites.get(sid);
  }

  /**
   * Get the number of sites in the database.
   * 
   * @return the number of sites
   */
  public int getSiteNumber() {
    return sites.size();
  }

  /**
   * Try to acquire a read lock on data item. If succeed, record the value read
   * from data. If fail, record the blocking transaction who is the oldest
   * holder of all locks regarding this data item;
   * 
   * @param xid
   *          the ID of the data item
   * @param transaction
   *          the transaction acquiring the lock
   * @return the response
   */
  @SuppressWarnings("unchecked")
  public AcquireLockResponse acquireReadLock(String xid,
      Transaction<E> transaction) {
    AcquireLockResponse response = new AcquireLockResponse(false);
    Site<E> freeSite = null;
    Lock lock = null;
    boolean allSitesDown = true;
    for (Site<E> site : dataToSites.get(xid)) {
      if (!site.isUp()) {
        continue;
      }
      if (site.getDataState(xid) != DataItemState.UP_TO_DATE) {
        continue;
      }
      allSitesDown = false;
      lock = site.getLock(xid);
      // No lock for data on this site, lucky! We'll take it!
      if (lock == null) {
        freeSite = site;
        continue;
      }
      // If a lock (whether read or write) is held by this transaction, then no
      // need to change it;
      if (lock.isHeldBy(transaction)) {
        response.value = site.getLatestData(xid).getValue();
        response.succeessful = true;
        return response;
      }
      // Write lock on this site...nothing we can do.
      if (lock.getType() == LockType.WRITE) {
        response.blocker = lock.getOldestHolder();
        return response;
      }
    }
    if (allSitesDown) {
      return response;
    }
    if (freeSite != null) {
      // If site with no lock on the data exists, we simply take this site and
      // place read lock.
      lock = new Lock(LockType.READ, freeSite, xid, transaction);
      freeSite.setLock(xid, lock);
    } else {
      // Otherwise we share a read lock with other transaction(s).
      assert (lock != null && lock.getType() == LockType.READ);
      lock.addHolder(transaction);
      freeSite = (Site<E>) lock.getSite();
    }
    transaction.acquiredLock(lock);
    transaction.addAccessedSites(freeSite, manager.getCurrentTime());
    response.value = freeSite.getLatestData(xid).getValue();
    response.succeessful = true;
    return response;
  }

  /**
   * Try to acquire write locks on data item. If succeed, record the original
   * value of the data being written. If fail, record the blocking transaction
   * who is the oldest holder of all locks regarding this data item,
   * 
   * @param xid
   *          the ID of the data item
   * @param transaction
   *          the transaction acquiring the lock
   * @return the response
   */
  public AcquireLockResponse acquireWriteLock(String xid,
      Transaction<E> transaction) {
    AcquireLockResponse response = new AcquireLockResponse(false);
    Lock lock = null;
    boolean allSitesDown = true;
    Transaction<E> oldestHolder = null;
    List<Lock> ownedLocks = new LinkedList<Lock>();
    // Check all sites holding this data;
    for (Site<E> site : dataToSites.get(xid)) {
      if (!site.isUp()) {
        continue;
      }
      allSitesDown = false;
      lock = site.getLock(xid);
      if (lock == null) {
        continue;
      }
      // If the lock is exclusively held by the transaction, we may want to keep
      // record of it (if we succeed in requiring locks from all site, we may
      // want to replace these lock with write locks).
      if (lock.isOwnedBy(transaction)) {
        ownedLocks.add(lock);
        lock = null;
      } else {
        // otherwise, find oldest transaction holding the locks we want;
        @SuppressWarnings("unchecked")
        Transaction<E> oldHolder = (Transaction<E>) lock.getOldestHolder();
        if (oldestHolder == null
            || oldestHolder.getArriveTime() > oldHolder.getArriveTime()) {
          oldestHolder = oldHolder;
        }
      }
    }
    if (allSitesDown) {
      return response;
    }
    if (lock != null) {
      response.blocker = oldestHolder;
      return response;
    }
    // No data are locked, or they're only locked by this transaction.
    for (Lock ownedLock : ownedLocks) {
      ownedLock.removeHolder(transaction);
    }
    // Now we place write locks on all live sites
    E value = null;
    for (Site<E> site : dataToSites.get(xid)) {
      if (!site.isUp()) {
        continue;
      }
      DataItem<E> data = site.getLatestData(xid);
      if (data != null) {
        value = data.getValue();
      }
      lock = new Lock(LockType.WRITE, site, xid, transaction);
      site.setLock(xid, lock);
      transaction.acquiredLock(lock);
      transaction.addAccessedSites(site, manager.getCurrentTime());
    }
    response.value = value;
    response.succeessful = true;
    return response;
  }

  /**
   * Get the data of a specific version.
   * 
   * @param xid
   *          the ID of the requested data
   * 
   * @param version
   *          the requested version
   * 
   * @return the DataItem of version
   */
  public DataItem<E> getDataOfVersion(String xid, long version) {
    for (Site<E> site : dataToSites.get(xid)) {
      if (!site.isUp()) {
        continue;
      }
      DataItem<E> data = site.getDataOfVersion(xid, version);
      if (data != null) {
        return data;
      }
    }
    return null;
  }

  /**
   * Drop all old versions on all sites (only when all sites are up).
   */
  public void dropVersionOld() {
    if (!areAllSitesUp()) {
      return;
    }
    dropVersionOlderThan(getCurrentVersion());
  }

  /**
   * Drop versions older than given version on all sites (only when all sites
   * are up).
   * 
   * @param version
   */
  public void dropVersionOlderThan(long version) {
    if (!areAllSitesUp()) {
      return;
    }
    for (Site<E> site : sites.values()) {
      site.dropVersionsOlderThan(version);
    }
    // Clear version-list of old versions;
    ArrayList<Integer> removeIndex = new ArrayList<Integer>();
    for (int i = 0; i < versions.size(); i++) {
      if (versions.get(i) < version) {
        removeIndex.add(i);
      } else {
        break;
      }
    }
    // Remove version data in reverse order so that it won't mass up.
    Collections.sort(removeIndex, Collections.reverseOrder());
    for (int index : removeIndex) {
      versions.remove(index);
    }
  }

  /**
   * Commit list of data written by some transaction. The new data will be
   * assigned a new version. Old versions will be purged according to oldest
   * read-only transaction.
   * 
   * @param dataToCommit
   *          list of DataItem to commit
   * @param newVersion
   *          the new version assigned to the committed data
   * @param oldestReadOnlyVersion
   *          the oldest version some read-only transaction needs to see
   */
  public void commitData(List<DataItem<E>> dataToCommit, Long newVersion,
      Long oldestReadOnlyVersion) {
    long currentVersion = getCurrentVersion();
    // Notify all sites of the new version;
    for (Site<E> site : sites.values()) {
      if (site.isUp()) {
        if (oldestReadOnlyVersion < currentVersion) {
          site.replaceLastestVersionOfData(newVersion);
        } else {
          site.addVersionOfData(newVersion);
        }
      }
    }
    // Modify changed data items in the new version;
    for (DataItem<E> data : dataToCommit) {
      for (Site<E> site : dataToSites.get(data.getId())) {
        if (site.isUp()) {
          site.writeDataInLatestVersion(new DataItem<E>(data.getId(), data
              .getValue(), site));
        }
      }
    }
    // Modify version-list accordingly;
    if (oldestReadOnlyVersion < currentVersion) {
      versions.pollLast();
    }
    versions.add(newVersion);
  }

  public long getCurrentVersion() {
    return versions.peekLast();
  }

  public boolean siteFail(String sid, long time) {
    if (sites.get(sid) == null) {
      logger.error("site '" + sid + "' doesn't exist.");
      return false;
    } else {
      sites.get(sid).goDown(time);
      return true;
    }
  }

  public boolean siteRecover(String sid) {
    if (sites.get(sid) == null) {
      logger.error("site '" + sid + "' doesn't exist.");
      return false;
    } else {
      sites.get(sid).goUp(versions.peekLast());
      return true;
    }
  }

  public boolean dataExists(String xid) {
    return dataToSites.get(xid) != null;
  }

  /**
   * Dump latest version of all data.
   * 
   * @return a list of the latest version of data
   */
  public List<DataItem<E>> dump() {
    long currentVersion = getCurrentVersion();
    return dumpVersion(currentVersion);
  }

  /**
   * Dump a specific version of all data.
   * 
   * @param version
   *          the version wanted to dump
   * @return a list of data of that version, or an empty list if the version
   *         doesn't exist.
   */
  public List<DataItem<E>> dumpVersion(long version) {
    List<DataItem<E>> result = new ArrayList<DataItem<E>>();
    if (!versions.contains(version)) {
      logger.error("version '" + version + "' does not exist.");
      return result;
    }
    List<String> dataIDs = new ArrayList<String>(dataToSites.keySet());
    Collections.sort(dataIDs, dataIDComparator);
    for (String xid : dataIDs) {
      List<Site<E>> sites = dataToSites.get(xid);
      // Get first up-to-data data;
      boolean achieved = false;
      for (Site<E> site : sites) {
        if (site.isUp()) {
          DataItem<E> data = site.getDataOfVersion(xid, version);
          if (data != null) {
            result.add(data);
            achieved = true;
            break;
          }
        }
      }
      if (!achieved) {
        result.add(new DataItem<E>(xid, null, null));
      }
    }
    return result;
  }

  /**
   * Dump all versions of all data.
   * 
   * @return a map from version (long) to list of DataItem
   */
  public HashMap<Long, List<DataItem<E>>> dumpAll() {
    HashMap<Long, List<DataItem<E>>> result = new HashMap<Long, List<DataItem<E>>>();
    for (Long version : versions) {
      result.put(version, dumpVersion(version));
    }
    return result;
  }

  /**
   * Get a data of latest version.
   * 
   * @param xid
   *          the ID of the requested data
   * @return the latest version of data, or null if no data can be reached.
   */
  public DataItem<E> getLatestData(String xid) {
    for (Site<E> site : dataToSites.get(xid)) {
      if (site.getDataState(xid) == DataItemState.UP_TO_DATE) {
        return site.getLatestData(xid);
      }
    }
    return null;
  }

  private boolean areAllSitesUp() {
    for (Site<E> site : sites.values()) {
      if (!site.isUp()) {
        return false;
      }
    }
    return true;
  }
}